Name:           packer
Version:        1.4.1
Release:        1%{?dist}
Summary:        Packer is a tool for creating identical machine images for multiple platforms from a single source configuration.
Group:          Applications/System
License:        MPLv2.0
URL:            https://packer.io/
Source0:        https://github.com/hashicorp/%{name}/archive/v%{version}.tar.gz
BuildRequires:  golang >= 1.11
BuildRequires:  make which zip
ExclusiveArch:  x86_64 i686 i586 i386


%description
Packer is a tool for creating identical machine images for multiple platforms
from a single source configuration.
Out of the box Packer comes with support to build images for Amazon EC2,
DigitalOcean, Google Compute Engine, QEMU, VirtualBox, VMware, and more.
Support for more platforms is on the way, and anyone can add new platforms via
plugins. 

%prep
%setup -q -n %{name}-%{version}

%build
%define debug_package %{nil}
export GOPATH=$PWD
export PATH=${PATH}:${GOPATH}/bin

%ifarch x86_64 amd64
  export XC_ARCH=amd64
%else
  export XC_ARCH=386
%endif

export XC_OS=linux
mkdir -p src/github.com/hashicorp/%{name}
shopt -s extglob dotglob
mv !(src) src/github.com/hashicorp/%{name}
shopt -u extglob dotglob
pushd src/github.com/hashicorp/%{name}
make bin
popd

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 0755 src/github.com/hashicorp/%{name}/bin/%{name} $RPM_BUILD_ROOT%{_bindir}

%clean
rm -rf %{buildroot}

%files
%license src/github.com/hashicorp/%{name}/LICENSE
%doc src/github.com/hashicorp/%{name}/{CHANGELOG.md,README.md}
%defattr(-,root,root,-)
%attr(755, root, root) %{_bindir}/%{name}*

%changelog
